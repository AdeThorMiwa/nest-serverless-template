service: bitpowr-xrp-wallet-service
frameworkVersion: '2'

custom:
  region: ${opt:region, self:provider.region}
  stage: ${opt:stage, self:provider.stage}
  address_table: ${self:service}-address-table-${opt:stage, self:provider.stage}
  wallet_table: ${self:service}-wallet-table-${opt:stage, self:provider.stage}
  transaction_table: ${self:service}-transaction-table-${opt:stage, self:provider.stage}
  table_throughputs:
    prod: 5
    default: 1
  table_throughput: ${self:custom.table_throughputs.${self:custom.stage},self:custom.table_throughputs.default}
  dynamodb:
    stages:
      - dev
    start:
      port: 8008
      inMemory: true
      heapInitial: 200m
      heapMax: 1g
      migrate: true
      seed: true
      convertEmptyValues: true
      # noStart: true
  serverless-offline:
    httpPort: 5000
    babelOptions:
      presets:
        - env

plugins:
  - serverless-plugin-typescript
  - serverless-plugin-optimize
  - serverless-offline
  - serverless-dotenv-plugin
  - serverless-dynamodb-local

provider:
  name: aws
  runtime: nodejs14.x
  stage: dev
  region: eu-west-2
  environment:
    AWS_NODEJS_CONNECTION_REUSE_ENABLED: 1
    REGION: ${self:custom.region}
    STAGE: ${self:custom.stage}
    RIPPLE_SERVER: wss://s.altnet.rippletest.net:51233
    ADDRESS_TABLE: ${self:custom.address_table}
    WALLET_TABLE: ${self:custom.wallet_table}
    TRANSACTION_TABLE: ${self:custom.transaction_table}
  iam:
    role:
      statements:
        - Effect: Allow
          Action:
            - 'dynamodb:DescribeTable'
            - 'dynamodb:Query'
            - 'dynamodb:Scan'
            - 'dynamodb:GetItem'
            - 'dynamodb:PutItem'
            - 'dynamodb:UpdateItem'
            - 'dynamodb:DeleteItem'
          Resource: '*'
  lambdaHashingVersion: 20201221

package:
  individually: true

functions:
  address:
    handler: src/modules/address/lambda.handler
    environment:
      MY_LOCAL_VAR: my-local-var
    events:
      - http:
          method: any
          path: /address/{proxy+}
  wallet:
    handler: src/modules/wallet/lambda.handler
    environment:
      MY_LOCAL_VAR: my-local-var
    events:
      - http:
          method: any
          path: /wallet/{proxy+}
  transaction:
    handler: src/modules/transaction/lambda.handler
    environment:
      MY_LOCAL_VAR: my-local-var
    events:
      - http:
          method: any
          path: /transaction/{proxy+}

resources:
  Resources:
    AddressTable:
      Type: 'AWS::DynamoDB::Table'
      DeletionPolicy: Retain
      Properties:
        TableName: ${self:provider.environment.ADDRESS_TABLE}
        AttributeDefinitions:
          - AttributeName: id
            AttributeType: S
          - AttributeName: address
            AttributeType: S
          - AttributeName: walletId
            AttributeType: S
        KeySchema:
          - AttributeName: id
            KeyType: HASH
        ProvisionedThroughput:
          ReadCapacityUnits: ${self:custom.table_throughput}
          WriteCapacityUnits: ${self:custom.table_throughput}
        GlobalSecondaryIndexes:
          - IndexName: address_index
            KeySchema:
              - AttributeName: address
                KeyType: HASH
            Projection:
              ProjectionType: ALL
            ProvisionedThroughput:
              ReadCapacityUnits: ${self:custom.table_throughput}
              WriteCapacityUnits: ${self:custom.table_throughput}
          - IndexName: wallet_index
            KeySchema:
              - AttributeName: walletId
                KeyType: HASH
            Projection:
              ProjectionType: ALL
            ProvisionedThroughput:
              ReadCapacityUnits: ${self:custom.table_throughput}
              WriteCapacityUnits: ${self:custom.table_throughput}

    WalletTable:
      Type: 'AWS::DynamoDB::Table'
      DeletionPolicy: Retain
      Properties:
        TableName: ${self:provider.environment.WALLET_TABLE}
        AttributeDefinitions:
          - AttributeName: id
            AttributeType: S
        KeySchema:
          - AttributeName: id
            KeyType: HASH
        ProvisionedThroughput:
          ReadCapacityUnits: ${self:custom.table_throughput}
          WriteCapacityUnits: ${self:custom.table_throughput}

    TransactionTable:
      Type: 'AWS::DynamoDB::Table'
      DeletionPolicy: Retain
      Properties:
        TableName: ${self:provider.environment.TRANSACTION_TABLE}
        AttributeDefinitions:
          - AttributeName: id
            AttributeType: S
          - AttributeName: addressId
            AttributeType: S
          - AttributeName: walletId
            AttributeType: S
        KeySchema:
          - AttributeName: id
            KeyType: HASH
        ProvisionedThroughput:
          ReadCapacityUnits: ${self:custom.table_throughput}
          WriteCapacityUnits: ${self:custom.table_throughput}
        GlobalSecondaryIndexes:
          - IndexName: address_index
            KeySchema:
              - AttributeName: addressId
                KeyType: HASH
            Projection:
              ProjectionType: ALL
            ProvisionedThroughput:
              ReadCapacityUnits: ${self:custom.table_throughput}
              WriteCapacityUnits: ${self:custom.table_throughput}
          - IndexName: wallet_index
            KeySchema:
              - AttributeName: walletId
                KeyType: HASH
            Projection:
              ProjectionType: ALL
            ProvisionedThroughput:
              ReadCapacityUnits: ${self:custom.table_throughput}
              WriteCapacityUnits: ${self:custom.table_throughput}

useDotenv: true
