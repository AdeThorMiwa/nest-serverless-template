import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  BadRequestException,
} from '@nestjs/common';
import { CreateWalletDto } from './dto/request.dto';
import WalletService from './service';
import AddressService from './../address/service';

@Controller('wallet')
class WalletController {
  constructor(
    private readonly walletService: WalletService,
    private readonly addressService: AddressService,
  ) {}

  @Post('/')
  async createWallet(@Body() body: CreateWalletDto): Promise<any> {
    if (body.mnemonics && body.privateKey) {
      throw new BadRequestException(
        'Please provide either a mnemonics phrase or privateKey.',
      );
    }

    try {
      const { id, type, mode, name } = await this.walletService.createWallet(
        body,
      );

      const { address, publicKey, restShard, metadata } =
        await this.addressService.createPrimaryAddress({
          walletId: id,
          metadata: body.metaData,
          password: body.password,
          encryptionKey: body.encryptionKey,
        });

      await this.walletService.updateWalletPrimaryAddress(id, address);

      // send address to monitor service for monitoring
      await this.addressService.sendToMonitorService(address);

      const response = {
        id,
        type,
        mode,
        name,
        address,
        m: body.m,
        n: body.n,
        metadata,
        publicKey,
        keyShard: restShard,
      };

      // send response
      return {
        statusCode: 201,
        body: response,
      };
    } catch (e) {
      console.log('Error', e);
      throw new BadRequestException(e.message);
    }
  }

  @Get('/')
  async getWallet(@Query('id') walletId: string): Promise<any> {
    if (!walletId) throw new BadRequestException('Please provide a wallet id');
    try {
      const wallet = await this.walletService.getWalletByIdWithTransaction(
        walletId,
      );

      return {
        statusCode: 200,
        body: wallet,
      };
    } catch (e) {
      console.log('Error:', e);
      throw new BadRequestException(e.message);
    }
  }

  @Get('/get-balance')
  async getWalletBalance(@Query('id') walletId: string): Promise<any> {
    if (!walletId) throw new BadRequestException('Please provide a wallet id');
    try {
      const addresses = await this.addressService.getAddressByWallet(walletId);
      const balances = await this.addressService.getBalanceOfAddresses(
        addresses,
      );

      return {
        statusCode: 200,
        data: balances,
      };
    } catch (e) {
      console.log('Error: ', e.message);
      throw new BadRequestException(e.message);
    }
  }

  @Get('/get-transactions')
  async getWalletTransactions(@Query('id') walletId: string): Promise<any> {
    if (!walletId) throw new BadRequestException('Please provide a wallet id');
    try {
      const transactions = await this.walletService.getTransactionsByWallet(
        walletId,
      );

      return {
        statusCode: 200,
        data: {
          transactions,
        },
      };
    } catch (e) {
      console.log('Error:', e.message);
      throw new BadRequestException(e.message);
    }
  }
}

export default WalletController;
