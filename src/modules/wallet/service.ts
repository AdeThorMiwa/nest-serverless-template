import { IWallet } from './../../interfaces/wallet';
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateWalletDto } from './dto/request.dto';
import ripple from './../../service/ripple';
import { encrypt } from './../../utils/crypto';
import { WalletDto } from './dto/entity.dto';
import database from './../../service/database';
import { v4 } from 'uuid';
import { WalletModes, WalletTypes } from '@enums/wallet';
import { ITransaction } from '@interfaces/transaction';
import TransactionService from './../transaction/service';

const WalletDB = new database(
  `${process.env.APP_NAME}-wallet-table-${process.env.STAGE}`,
);

//TODO: FIXME: seperate each task(i.e db transactions) to correlating service

@Injectable()
class WalletService {
  constructor(private readonly transactionService: TransactionService) {}

  async createWallet({
    name,
    type,
    mode,
    password,
    mnemonics,
  }: CreateWalletDto): Promise<IWallet> {
    const _mnemonics = encrypt(mnemonics, password) as string; //FIXME: unencrypt mnemonics

    console.log('post wallet DB');

    const wallet = await WalletDB.create<WalletDto, IWallet>(
      new WalletDto({
        name: name || `address-${v4()}`,
        primaryAddress: '0',
        mode: mode || WalletModes.HOT,
        type: type || WalletTypes.HD,
        mnemonics: _mnemonics,
      }),
    );

    return wallet;
  }

  async updateWalletPrimaryAddress(
    walletId: string,
    address: string,
  ): Promise<void> {
    await WalletDB.updateById(walletId, { primaryAddress: address });
  }

  async getWalletById(id: string) {
    const wallet = await WalletDB.getById<IWallet>(id);

    if (!wallet) throw new NotFoundException('Wallet not found.');

    return wallet;
  }

  async getWalletByIdWithTransaction(id: string) {
    const wallet = await this.getWalletById(id);
    const transactions = await this.transactionService.getTransactionsByWallet(
      wallet.id,
    );

    return {
      id: wallet.id,
      mode: wallet.mode,
      type: wallet.type,
      address: wallet.primaryAddress,
      transactionCount: transactions.length,
    };
  }

  async getTransactionsByWallet(walletId: string): Promise<ITransaction[]> {
    return await this.transactionService.getTransactionsByWallet(walletId);
  }
}

export default WalletService;
