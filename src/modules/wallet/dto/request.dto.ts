import { WalletModes, WalletTypes } from './../../../enums/wallet';
import {
  IsAlphanumeric,
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsObject,
  IsOptional,
  IsString,
  Max,
  MaxLength,
  Min,
  MinLength,
} from 'class-validator';

class CreateWalletDto {
  @IsOptional()
  @IsEnum(WalletTypes)
  readonly type: WalletTypes;

  @IsOptional()
  @IsEnum(WalletModes)
  readonly mode: WalletModes;

  @IsString()
  @IsNotEmpty({ message: 'Please provide an encryptionKey' })
  readonly encryptionKey: string;

  @IsOptional()
  @IsString()
  readonly mnemonics: string;

  @IsOptional()
  @IsString()
  readonly privateKey: string;

  @IsNotEmpty({ message: 'Please provide a password' })
  @IsAlphanumeric()
  @MinLength(8, {
    message: 'Please provide a password greater than 8 characters.',
  })
  @MaxLength(15, {
    message: 'Please provide a password less than 15 characters.',
  })
  readonly password: string;

  @IsOptional()
  @IsString()
  @MinLength(2)
  readonly name?: string;

  @IsInt()
  @Min(3)
  @Max(200)
  @IsOptional()
  readonly m: string;

  @IsInt()
  @IsOptional()
  readonly n: string;

  @IsOptional()
  @IsObject()
  readonly metaData: Record<string, unknown>;
}

export { CreateWalletDto };
