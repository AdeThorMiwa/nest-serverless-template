import { WalletModes, WalletTypes } from './../../../enums/wallet';
import { IWallet } from './../../../interfaces/wallet';
import {
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsString,
  IsUUID,
  MinLength,
} from 'class-validator';
import { v4 } from 'uuid';

class WalletDto implements IWallet {
  @IsUUID()
  readonly id: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(2)
  readonly name: string;

  @IsEnum(WalletTypes)
  readonly type: WalletTypes;

  @IsEnum(WalletModes)
  readonly mode: WalletModes;

  @IsString()
  readonly primaryAddress: string;

  @IsNotEmpty()
  @IsString()
  readonly mnemonics: string;

  constructor({
    name,
    primaryAddress,
    type,
    mode,
    mnemonics,
  }: Omit<IWallet, 'id'>) {
    console.log('name ==========================', name);
    this.id = v4();
    this.name = name;
    this.type = type;
    this.mode = mode;
    this.primaryAddress = primaryAddress;
    this.mnemonics = mnemonics;
  }
}

export { WalletDto };
