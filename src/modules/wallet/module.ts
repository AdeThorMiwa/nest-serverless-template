import { Module } from '@nestjs/common';
import WalletController from './controller';
import WalletService from './service';
import AddressService from './../address/service';
import TransactionService from './../transaction/service';
import VaultService from './../../service/vault';

@Module({
  controllers: [WalletController],
  providers: [WalletService, AddressService, TransactionService, VaultService],
})
export default class WalletModule {}
