import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Post,
  Query,
} from '@nestjs/common';
import { EstimateFeesDto, NewTransactionDto } from './dto/validator.dto';
import TransactionService from './service';
import AddressService from './../address/service';
import VaultService from './../../service/vault';
import { decrypt } from './../../utils/crypto';

@Controller('transaction')
class TransactionController {
  constructor(
    private readonly transactionService: TransactionService,
    private readonly addressService: AddressService,
    private readonly vaultService: VaultService,
  ) {}

  @Post('/')
  async newTransaction(@Body() body: NewTransactionDto): Promise<any> {
    const { decryptionKey, from, to, amount, publicKey, keyShard } = body;
    const address = await this.addressService.getAddress(from);

    console.log('ca n =====================');

    const secret = await this.vaultService.restoreSecretFromVault(
      decryptionKey,
      {
        kid: decrypt(address.kid, body.password) as string,
        publicKey: publicKey,
        shards: [...keyShard, address.keyShard],
      },
    );

    console.log('secret a key ======', secret);

    // prepare transaction
    const preparedTransaction =
      await this.transactionService.prepareTransaction({
        from,
        to,
        amount,
      });

    console.log('prepared ==', preparedTransaction);

    //sign transaction
    const signed = await this.transactionService.signTransaction(
      preparedTransaction,
      secret,
    );

    console.log('signed ====', signed);

    // submit transaction
    const submit = await this.transactionService.submitTransaction(
      signed.signedTransaction,
    );

    console.log(submit);

    if (submit.resultCode !== 'tesSUCCESS')
      throw new BadRequestException(
        'Something went wrong while creating this transaction',
      );

    const transaction = await this.transactionService.newTransaction({
      ...(submit as any).tx_json,
      earliestLedgerVersion: submit.earliestLedgerVersion,
      addressId: address.id,
      walletId: address.walletId,
    });

    return {
      statusCode: 201,
      data: transaction,
    };
  }

  @Get('/')
  async getTransaction(@Query('id') transactionId: string): Promise<any> {
    if (!transactionId)
      throw new BadRequestException('Please provide a wallet id');
    const transaction = await this.transactionService.getTransaction(
      transactionId,
    );

    return {
      statusCode: 200,
      data: transaction,
    };
  }

  @Post('/estimate-fees')
  async estimateTransactionFee(@Body() body: EstimateFeesDto): Promise<any> {
    const { from, to, amount } = body;
    await this.addressService.getAddress(from);

    // prepare transaction
    const prepared = await this.transactionService.prepareTransaction({
      to,
      from,
      amount,
    });

    const estimatedFee = JSON.parse(prepared.txJSON);

    return {
      statusCode: 200,
      data: { ...estimatedFee, fees: Number(estimatedFee.fee) / 1000000 },
    };
  }
}

export default TransactionController;
