import { ITransaction, ITransactionMandates } from '@interfaces/transaction';
import { Injectable } from '@nestjs/common';
import ripple from './../../service/ripple';
import database from './../../service/database';
import { TransactionDto } from './dto/entity.dto';
import { Prepare } from 'ripple-lib';
import { v4 } from 'uuid';

const TransactionDB = new database(
  `${process.env.APP_NAME}-transaction-table-${process.env.STAGE}`,
);

@Injectable()
class TransactionService {
  async getTransactions(
    opts: Record<string, unknown>,
    settings: { useIndex: string },
  ): Promise<ITransaction[]> {
    return await TransactionDB.getAll<ITransaction>(opts, settings);
  }

  async getTransaction(transactionId: string): Promise<ITransaction> {
    return await TransactionDB.getById<ITransaction>(transactionId);
  }

  async getTransactionsByWallet(walletId: string): Promise<ITransaction[]> {
    return await this.getTransactions(
      { walletId },
      { useIndex: 'wallet_index' },
    );
  }

  async getTransactionsByAddress(addressId: string): Promise<ITransaction[]> {
    return await this.getTransactions(
      { addressId },
      { useIndex: 'address_index' },
    );
  }

  async prepareTransaction(trx: ITransactionMandates) {
    await ripple.init();
    return await ripple.prepare(trx);
  }

  async signTransaction(prepared: Prepare, secret: string) {
    return await ripple.sign(prepared, secret);
  }

  async submitTransaction(signedTransaction: string) {
    return await ripple.submit(signedTransaction);
  }

  async newTransaction(trx: Record<string, unknown>) {
    return await TransactionDB.create<any, ITransaction>({ id: v4(), ...trx });
  }
}

export default TransactionService;
