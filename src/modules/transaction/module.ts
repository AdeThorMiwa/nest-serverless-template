import { Module } from '@nestjs/common';
import TransactionController from './controller';
import TransactionService from './service';
import AddressService from './../address/service';
import WalletService from './../wallet/service';
import VaultService from './../../service/vault';

@Module({
  controllers: [TransactionController],
  providers: [TransactionService, AddressService, WalletService, VaultService],
})
export default class TransactionModule {}
