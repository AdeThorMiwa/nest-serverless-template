import { Server } from 'http';
import bootstrapServer from '../../bootstrap';
import { Handler, Context, APIGatewayEvent } from 'aws-lambda';
import { proxy } from 'aws-serverless-express';
import TransactionModule from './module';

let cachedServer: Server;

export const handler: Handler = async (
  event: APIGatewayEvent,
  context: Context,
) => {
  cachedServer = await bootstrapServer(TransactionModule);
  return proxy(cachedServer, event, context, 'PROMISE').promise;
};
