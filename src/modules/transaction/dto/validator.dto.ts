import {
  IsAlphanumeric,
  IsArray,
  IsInt,
  IsNotEmpty,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

class AddressAndAmountBaseDto {
  @IsNotEmpty()
  @IsString()
  readonly from: string;

  @IsNotEmpty()
  @IsString()
  readonly to: string;

  @IsInt()
  readonly amount: number;
}

class NewTransactionDto extends AddressAndAmountBaseDto {
  @IsNotEmpty()
  @IsString()
  decryptionKey: string;

  @IsNotEmpty()
  @IsString()
  readonly publicKey: string;

  @IsNotEmpty()
  @IsArray()
  readonly keyShard: string;

  @IsNotEmpty({ message: 'Please provide a password' })
  @IsAlphanumeric()
  @MinLength(8, {
    message: 'Please provide a password greater than 8 characters.',
  })
  @MaxLength(15, {
    message: 'Please provide a password less than 15 characters.',
  })
  readonly password: string;
}

class EstimateFeesDto extends AddressAndAmountBaseDto {}

export { NewTransactionDto, EstimateFeesDto };
