import { TransactionTypes } from './../../../enums/transaction';
import { ITransaction } from './../../../interfaces/transaction';
import {
  IsNotEmptyObject,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { v4 } from 'uuid';

class TransactionDto implements ITransaction {
  @IsUUID()
  id: string;

  @IsString()
  from: string;

  @IsString()
  to: string;

  amount: string;

  hash: string;

  type: TransactionTypes;

  fee: string;

  flags: number;

  @IsString()
  addressId: string;

  @IsUUID(4, { message: 'Please provide a valid wallet ID' })
  walletId: string;

  constructor({ from }: Omit<ITransaction, 'id'>) {
    this.id = v4();
    this.from = from;
  }
}

export { TransactionDto };
