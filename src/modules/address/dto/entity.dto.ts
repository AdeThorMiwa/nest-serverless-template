import { IAddress } from './../../../interfaces/address';
import { IWallet } from './../../../interfaces/wallet';
import {
  IsNotEmpty,
  IsNotEmptyObject,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { v4 } from 'uuid';

class AddressDto implements IAddress {
  @IsUUID()
  readonly id: string;

  @IsString()
  readonly address: string;

  @IsString()
  readonly xAddress: string;

  @IsUUID(4, { message: 'Please provide a valid wallet ID' })
  readonly walletId: string;

  @IsOptional()
  @IsNotEmptyObject()
  readonly wallet?: IWallet;

  @IsNotEmpty()
  @IsString()
  readonly kid: string;

  @IsNotEmpty()
  @IsString()
  readonly keyShard: string;

  @IsNotEmpty()
  @IsString()
  readonly publicKey: string;

  @IsNotEmptyObject()
  readonly metadata: Record<string, unknown>;

  @IsOptional()
  @IsString()
  readonly description: string;

  constructor({
    address,
    xAddress,
    walletId,
    metadata,
    description,
    keyShard,
    kid,
    publicKey,
  }: Omit<IAddress, 'id'>) {
    this.id = v4();
    this.address = address;
    this.xAddress = xAddress;
    this.walletId = walletId;
    this.metadata = metadata;
    this.description = description;
    this.keyShard = keyShard;
    this.kid = kid;
    this.publicKey = publicKey;
  }
}

export { AddressDto };
