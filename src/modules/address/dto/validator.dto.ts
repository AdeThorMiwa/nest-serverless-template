import {
  IsAlphanumeric,
  IsNotEmpty,
  IsObject,
  IsOptional,
  IsString,
  IsUUID,
  MaxLength,
  MinLength,
} from 'class-validator';

class GenerateAddressDto {
  @IsUUID(4, { message: 'Please provide a valid wallet ID' })
  walletId: string;

  @IsOptional()
  @IsString({ message: 'Please provide a description for this address' })
  description: string;

  @IsString()
  @IsNotEmpty({ message: 'Please provide an encryptionKey' })
  readonly encryptionKey: string;

  @IsNotEmpty({ message: 'Please provide a password' })
  @IsAlphanumeric()
  @MinLength(8, {
    message: 'Please provide a password greater than 8 characters.',
  })
  @MaxLength(15, {
    message: 'Please provide a password less than 15 characters.',
  })
  readonly password: string;

  @IsOptional()
  @IsObject()
  metadata: Record<string, unknown>;
}

export { GenerateAddressDto };
