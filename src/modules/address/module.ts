import { Module } from '@nestjs/common';
import AddressController from './controller';
import AddressService from './service';
import WalletService from './../wallet/service';
import TransactionService from './../transaction/service';
import VaultService from './../../service/vault';

@Module({
  controllers: [AddressController],
  providers: [AddressService, WalletService, TransactionService, VaultService],
})
export default class AddressModule {}
