import { IAddress, IBalance } from '@interfaces/address';
import { Injectable, NotFoundException } from '@nestjs/common';
import ripple from './../../service/ripple';
import database from './../../service/database';
import { AddressDto } from './dto/entity.dto';
import { encrypt } from './../../utils/crypto';
import VaultService from './../../service/vault';
// import axios from 'axios';

const AddressDB = new database(
  `${process.env.APP_NAME}-address-table-${process.env.STAGE}`,
);

@Injectable()
class AddressService {
  constructor(private readonly vaultService: VaultService) {}
  async sendToMonitorService(address: string): Promise<void> {
    // await axios.post(
    //   process.env.MONITOR_URL,
    //   JSON.stringify({
    //     address,
    //     chain: 'XRP',
    //     webhook_url: process.env.EVENT_HANDLER_URL,
    //     priority: 1,
    //   }),
    // );
    console.log('sending address ' + address + 'to monitor...');
  }

  async createAddress({
    walletId,
    metadata,
    description,
    password,
    encryptionKey,
  }: {
    walletId: string;
    metadata: Record<string, unknown>;
    description: string;
    password: string;
    encryptionKey: string;
  }): Promise<IAddress & { restShard: string[] }> {
    await ripple.init();
    // create wallet
    const { secret, classicAddress, xAddress } = await ripple.generateAddress();

    console.log('address: ===========', { secret, classicAddress, xAddress });

    // send wallet to vault service for encryption
    const { kid, publicKey, shards } =
      await this.vaultService.sendKeyToVaultAndEncrypt(secret, encryptionKey);

    const [keyShard, ...rest] = shards;

    const address = await AddressDB.create<AddressDto, IAddress>(
      new AddressDto({
        metadata,
        walletId,
        address: classicAddress,
        xAddress,
        description,
        keyShard,
        kid: encrypt(kid, password) as string,
        publicKey,
      }),
    );

    return { ...address, restShard: rest };
  }

  async createPrimaryAddress(params: {
    walletId: string;
    metadata: Record<string, unknown>;
    password: string;
    encryptionKey: string;
  }): Promise<IAddress & { restShard: string[] }> {
    return await this.createAddress({
      ...params,
      description: 'Primary Address',
    });
  }

  async getAddressById(id: string): Promise<IAddress> {
    const address = await AddressDB.getById<IAddress>(id);
    if (!address) throw new NotFoundException('Address does not exist');
    return address;
  }

  async getAddress(_address: string): Promise<IAddress> {
    const address = await AddressDB.getAll<IAddress>(
      {
        address: _address,
      },
      { useIndex: 'address_index' },
    );

    if (!address.length) throw new NotFoundException('Address does not exist');

    return address[0];
  }

  async getAddressByWallet(walletId: string): Promise<IAddress[]> {
    return await AddressDB.getAll<IAddress>(
      {
        walletId,
      },
      { useIndex: 'wallet_index' },
    );
  }

  async getBalanceOfAddresses(
    addresses: IAddress[],
  ): Promise<{ [key: string]: IBalance[] }> {
    const balancesPromise = addresses.map(
      async (address) => await this.getAddressBalance(address.address),
    );

    return (await Promise.all(balancesPromise)).reduce(
      (prev, current, cIndex) => ({
        ...prev,
        [addresses[cIndex].address]: current,
      }),
      {},
    );
  }

  async getAddressBalance(address: string): Promise<IBalance> {
    await ripple.init();
    return (await ripple.getBalance(address))[0];
  }
}

export default AddressService;
