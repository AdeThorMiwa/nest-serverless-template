import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Post,
  Query,
} from '@nestjs/common';
import AddressService from './service';
import ripple from './../../service/ripple';
import { GenerateAddressDto } from './dto/validator.dto';
import WalletService from './../wallet/service';
import TransactionService from './../transaction/service';

@Controller('address')
class AddressController {
  constructor(
    private readonly addressService: AddressService,
    private readonly walletService: WalletService,
    private readonly transactionService: TransactionService,
  ) {}

  @Post('/generate')
  async generateAddress(@Body() body: GenerateAddressDto): Promise<any> {
    const { walletId, description, metadata, password, encryptionKey } = body;
    try {
      const wallet = await this.walletService.getWalletById(walletId);

      const newAddress = await this.addressService.createAddress({
        walletId: wallet.id,
        description,
        metadata,
        password,
        encryptionKey,
      });

      return {
        statusCode: 201,
        data: {
          ...newAddress,
        },
      };
    } catch (e) {
      console.log('Error: ', e);
      throw new BadRequestException(e.message);
    }
  }

  @Get('/')
  async getAddress(@Query('id') addressId: string): Promise<any> {
    if (!addressId)
      throw new BadRequestException('Please provide an address ID');
    const address = await this.addressService.getAddressById(addressId);
    return {
      statusCode: 200,
      data: address,
    };
  }

  @Get('/get-balance')
  async getAddressBalance(@Query('id') addressId: string): Promise<any> {
    if (!addressId)
      throw new BadRequestException('Please provide an address ID');
    const address = await this.addressService.getAddressById(addressId);
    const balance = await this.addressService.getAddressBalance(
      address.address,
    );

    return {
      statusCode: 200,
      data: balance,
    };
  }

  @Get('/get-transactions')
  async getAddressTransactions(@Query('id') addressId: string): Promise<any> {
    if (!addressId)
      throw new BadRequestException('Please provide an address ID');
    const transactions = await this.transactionService.getTransactionsByAddress(
      addressId,
    );

    return {
      statusCode: 200,
      data: {
        transactions,
      },
    };
  }
}

export default AddressController;
