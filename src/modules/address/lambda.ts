import { Server } from 'http';
import bootstrapServer from '../../bootstrap';
import { Handler, Context, APIGatewayEvent } from 'aws-lambda';
import { proxy } from 'aws-serverless-express';
import AddressModule from './module';

let cachedServer: Server;

export const handler: Handler = async (
  event: APIGatewayEvent,
  context: Context,
) => {
  cachedServer = await bootstrapServer(AddressModule);
  return proxy(cachedServer, event, context, 'PROMISE').promise;
};
