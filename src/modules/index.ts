import { Module } from '@nestjs/common';
import AddressModule from './address/module';
import WalletModule from './wallet/module';
import TransactionModule from './transaction/module';

@Module({
  imports: [AddressModule, WalletModule, TransactionModule],
})
export default class Modules {}
