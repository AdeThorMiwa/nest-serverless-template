export enum WalletTypes {
  HD = 'HD',
  MULTISIG = 'MULTISIG',
}

export enum WalletModes {
  HOT = 'HOT',
  COLD = 'COLD',
}
