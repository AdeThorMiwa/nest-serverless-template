import { TransactionTypes } from './../enums/transaction';

export interface ITransaction {
  id: string;
  hash: string;
  from: string;
  to: string;
  type: TransactionTypes;
  addressId: string;
  walletId: string;
}

export interface ITransactionMandates {
  from: string;
  to: string;
  amount: number;
}
