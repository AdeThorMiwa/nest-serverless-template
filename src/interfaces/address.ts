import { IWallet } from './wallet';

export interface IAddress {
  id: string;
  address: string;
  xAddress: string;
  walletId: string;
  wallet?: IWallet;
  metadata: Record<string, unknown>;
  kid: string;
  keyShard: string;
  publicKey: string;
  description: string;
}

export interface IBalance {
  value: string | number;
  currency: string;
}
