import { WalletModes, WalletTypes } from './../enums/wallet';

export interface IWallet {
  id: string;
  name: string;
  type: WalletTypes;
  mode: WalletModes;
  primaryAddress: string;
  mnemonics: string;
}
