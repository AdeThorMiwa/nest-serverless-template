import CryptoJS from 'crypto-js';

export const encrypt = (
  data: string,
  key: string,
  toString = true,
): string | CryptoJS.lib.CipherParams => {
  const encrypted = CryptoJS.TripleDES.encrypt(data, key);
  return toString ? encrypted.toString() : encrypted;
};

export const decrypt = (
  data: string,
  key: string,
  toString = true,
): string | CryptoJS.lib.WordArray => {
  const decrypted = CryptoJS.TripleDES.decrypt(data, key);
  return toString ? decrypted.toString(CryptoJS.enc.Utf8) : decrypted;
};
