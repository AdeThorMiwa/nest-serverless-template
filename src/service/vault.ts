import { Injectable } from '@nestjs/common';
import axios from 'axios';

@Injectable()
class VaultService {
  async sendKeyToVaultAndEncrypt(
    secret: string,
    key: string,
  ): Promise<{ kid: string; publicKey: string; shards: string[] }> {
    const {
      data: { data: secured },
    } = await axios.post(
      `${process.env.VAULT_URL}/encrypt`,
      {
        secret,
        number_of_shares: 3,
        required_shares: 2,
      },
      {
        headers: {
          'X-Api-Key': key,
          Authorization: 'allow',
        },
      },
    );

    return secured;
  }

  async restoreSecretFromVault(
    key: string,
    decryptData: {
      kid: string;
      publicKey: string;
      shards: string[];
    },
  ): Promise<string> {
    console.log(decryptData);
    const res = await axios.post(
      `${process.env.VAULT_URL}/decrypt`,
      decryptData,
      {
        headers: {
          'X-Api-Key': key,
          Authorization: 'allow',
        },
      },
    );
    return res.data.data.secret;
  }
}

export default VaultService;
