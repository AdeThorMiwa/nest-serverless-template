import { DynamoDB, config } from 'aws-sdk';

config.update({
  region: process.env.REGION,
  accessKeyId: process.env.AWS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});

type QueryInput = DynamoDB.DocumentClient.QueryInput;
type PutOutput = DynamoDB.DocumentClient.PutItemOutput;
type UpdateOutput = DynamoDB.DocumentClient.UpdateItemOutput;

const isProd = process.env.STAGE === 'prod';

class DBService {
  private documentClient: DynamoDB.DocumentClient;
  private table: string;

  constructor(table: string) {
    this.documentClient = new DynamoDB.DocumentClient({
      endpoint: isProd
        ? undefined
        : `http://localhost:${process.env.DYNAMO_PORT}`,
    });

    this.table = table;
  }

  async getAll<I = any>(
    params: Record<string, unknown>,
    opts?: { useIndex: string },
  ): Promise<I[]> {
    const { expression, ExpressionAttributeValues } =
      this.computeUpdateExpressionAndAttributes(params, false);

    console.log(expression, ExpressionAttributeValues);

    const queryParams: QueryInput = {
      TableName: this.table,
      KeyConditionExpression: expression,
      ExpressionAttributeValues,
    };

    if (opts && opts.useIndex) {
      queryParams['IndexName'] = opts.useIndex;
    }

    const doc = await this.documentClient.query(queryParams).promise();

    return doc.Items as I[];
  }

  async getById<I = any>(id: string): Promise<I> {
    const doc = await this.documentClient
      .get({
        TableName: this.table,
        Key: { id },
      })
      .promise();
    return doc.Item as I;
  }

  async create<I, R = any>(Item: I): Promise<R> {
    console.log(Object.keys(Item));
    const doc: PutOutput = await this.documentClient
      .put({
        TableName: this.table,
        Item,
        ReturnValues: 'ALL_OLD',
      })
      .promise();
    return (Object.keys(doc).length ? doc : Item) as R;
  }

  async updateById<I = any>(
    id: string,
    fields: Record<string, unknown>,
  ): Promise<I> {
    const { expression: UpdateExpression, ExpressionAttributeValues } =
      this.computeUpdateExpressionAndAttributes(fields);

    const updateParams = {
      TableName: this.table,
      Key: {
        id,
      },
      UpdateExpression,
      ExpressionAttributeValues,
      ReturnValues: 'ALL_NEW',
    };

    const doc: UpdateOutput = await this.documentClient
      .update(updateParams)
      .promise();
    return doc.Attributes as I;
  }

  deleteById(id: string): void {
    console.log(id);
  }

  private computeUpdateExpressionAndAttributes(
    fields: Record<string, unknown>,
    set = true,
  ): {
    expression: string;
    ExpressionAttributeValues: Record<string, unknown>;
  } {
    let expression = set ? 'set ' : '';
    const ExpressionAttributeValues: Record<string, unknown> = {};

    let i = 0;
    let key: string;
    for (key in fields) {
      expression += `${key} = :${key}${
        Object.keys(fields).length !== ++i ? ',' : ''
      }`;
      ExpressionAttributeValues[`:${key}`] = fields[key];
    }

    return { expression, ExpressionAttributeValues };
  }
}

export default DBService;
