import { IBalance } from '@interfaces/address';
import { ITransactionMandates } from '@interfaces/transaction';
import { BadRequestException } from '@nestjs/common';
import { Prepare, RippleAPI } from 'ripple-lib';

class RippleAPIService {
  private readonly provider: RippleAPI;
  initialized = false;

  constructor() {
    this.provider = new RippleAPI({
      server: process.env.RIPPLE_SERVER,
      connectionTimeout: 10000,
    });
  }

  requireInitialization(): void {
    if (!this.initialized) {
      throw new Error('Ripple Service not initialized.');
    }
  }

  async init() {
    if (!this.initialized) {
      console.log('connecting to ripple server...');
      await this.provider.connect();
      this.initialized = true;
    }
    return this;
  }

  async generateAddress() {
    this.requireInitialization();
    return this.provider.generateXAddress({
      test: process.env.NET === 'test',
      includeClassicAddress: true,
    });
  }

  async getBalance(address: string): Promise<IBalance[]> {
    this.requireInitialization();
    try {
      return await this.provider.getBalances(address);
    } catch (e) {
      if (e.message == 'Account not found.')
        return [
          {
            value: '0',
            currency: 'XRP',
          },
        ];
    }
  }

  async prepare({ from, to, amount }: ITransactionMandates) {
    this.requireInitialization();
    if (
      !this.provider.isValidAddress(from) ||
      !this.provider.isValidAddress(to)
    )
      throw new BadRequestException('Invalid (from) or (to) address');
    return await this.provider.prepareTransaction(
      {
        TransactionType: 'Payment',
        Account: from,
        Amount: this.provider.xrpToDrops(amount),
        Destination: to,
      },
      {
        maxLedgerVersionOffset: 75,
      },
    );
  }

  async sign(preparedTransaction: Prepare, secret: string) {
    this.requireInitialization();
    return this.provider.sign(preparedTransaction.txJSON, secret);
  }

  async submit(signed: string) {
    this.requireInitialization();
    const earliestLedgerVersion = (await this.provider.getLedgerVersion()) + 1;
    const tentativeResult = await this.provider.submit(signed);
    return { ...tentativeResult, earliestLedgerVersion };
  }

  async diconnect() {
    await this.provider.disconnect();
  }
}

export default new RippleAPIService();
